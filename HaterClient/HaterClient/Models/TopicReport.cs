﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class TopicReport
    {
        public int TopicReportId { get; set; }
        public int UserId { get; set; }
       
        public ApplicationUser User { get; set; }
        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Reason { get; set; }
        public string Verdict { get; set; }
        public bool Active { get; set; }
    }
}