﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HaterClient.Models.Enums;

namespace HaterClient.Models
{
    public class ApplicationUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime Birthday { get; set; }
        public string Description { get; set; }
        public Gender Gender { get; set; }
        public DateTime? LastActive { get; set; }
        public State State { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public int? PreferenceAgeStart { get; set; }
        public int? PreferenceAgeStop { get; set; }
        public float? PreferenceRadius { get; set; }
        public float? PreferenceMatchProtsent { get; set; }
        public List<Topic> Topics { get; set; }
        public List<UserTopicChoice> UserTopicChoices { get; set; }
        public List<TopicReport> TopicReports { get; set; }
       
        public List<UserMatch> UserInUserMatches { get; set; }
       
        public List<UserMatch> MatchInUserMatches { get; set; }
      
        public List<UserMessage> UserMessageSender { get; set; }
        
        public List<UserMessage> UserMessageReciever { get; set; }
        
        public List<UserReport> UserReportReporter { get; set; }
        
        public List<UserReport> UserReportReported { get; set; }
        public List<string> PreferredGenders { get; set; }

        public int Age()
        {
            DateTime n = DateTime.Now;
            int age = n.Year - Birthday.Year;

            if (n.Month < Birthday.Month || (n.Month == Birthday.Month && n.Day < Birthday.Day))
                age--;

            return age;
        }
    }
}