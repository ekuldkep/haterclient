﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class Topic
    {
        public int TopicId { get; set; }
        public string Description { get; set; }
        public bool Approved { get; set; }
        public string CreatedById { get; set; }
        public DateTime? CreatedAt { get; set; }
        //public List<UserTopicChoice> UserTopicChoices { get; set; }
        //public List<TopicReport> TopicReports { get; set; }

    }
}