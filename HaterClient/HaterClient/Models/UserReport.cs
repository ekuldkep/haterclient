﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class UserReport
    {
        public int UserReportId { get; set; }
        public int ReporterId { get; set; }
       
        public ApplicationUser Reporter { get; set; }
        public int ReportedId { get; set; }
        
        public ApplicationUser Reported { get; set; }
        public bool Active { get; set; }
        public DateTime Created { get; set; }
        public string Reason { get; set; }
        public string Verdict { get; set; }
    }
}