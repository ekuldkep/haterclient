﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public List<ApplicationUser> ApplicationUsers { get; set; }
    }
}