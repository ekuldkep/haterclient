﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class UserMatch
    {
        public int UserMatchId { get; set; }
        public DateTime Created { get; set; }
        public bool Active { get; set; }
        public int UserId { get; set; }
       
        public ApplicationUser User { get; set; }
        public int MatchId { get; set; }
        
        public ApplicationUser Match { get; set; }
    }
}