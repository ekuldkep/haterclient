﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models.Enums
{
    public enum Gender
    {
        Male = 0,
        Female = 1,
        Other = 2,
        ApacheHelicopter = 3
    }
}