﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models.Enums
{
    public enum State
    {
        Active = 0,
        Restricted = 1,
        Disabled = 2
    }
}