﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HaterClient.Models
{
    public class UserMessage
    {
        public int UserMessageId { get; set; }
        public int SenderId { get; set; }
        
        public ApplicationUser Sender { get; set; }
        public int ReceiverId { get; set; }
        
        public ApplicationUser Receiver { get; set; }
        public string Message { get; set; }
        public DateTime Created { get; set; }
    }
}