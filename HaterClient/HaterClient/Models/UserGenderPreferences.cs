﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HaterClient.Models.Enums;

namespace HaterClient.Models
{
    public class UserGenderPreferences
    {
        public int UserGenderPreferencesId { get; set; }
        public int ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public Gender Gender { get; set; }
    }
}