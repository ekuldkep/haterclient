﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HaterClient.Models.Enums;

namespace HaterClient.Models
{
    public class UserTopicChoice
    {
        public int UserTopicChoiceId { get; set; }

        public int UserId { get; set; }
        
        public ApplicationUser User { get; set; }

        public int TopicId { get; set; }
        public virtual Topic Topic { get; set; }
        public Choice Choice { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}