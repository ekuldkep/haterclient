﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HaterClient.Services;
using HaterClient.ViewModels;

namespace HaterClient.Controllers
{
    public class MatchController : Controller
    {
        private readonly MatchService _matchService = new MatchService();
        
        public async Task<ActionResult> Index()
        {
            GetToken();
            if (ViewBag.Token == null || string.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new MatchViewModel();

            try
            {
                vm = await _matchService.GetMatch(ViewBag.Token);
            }
            catch (Exception e)
            {
                // ignored
            }

            return View(vm);
        }

        public async Task<ActionResult> Pending()
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new List<MatchUserViewModel>();

            try
            {
                vm = await _matchService.GetPending(ViewBag.Token);
            }
            catch (Exception e)
            {
                // ignored
            }

            return View(vm);
        }

        public string GetToken()
        {
            HttpCookie token = this.Request.Cookies["token"];
            if (token != null)
            {
                ViewBag.Token = token.Value;
                return token.Value;
            }
            return null;
        }
    }
}
