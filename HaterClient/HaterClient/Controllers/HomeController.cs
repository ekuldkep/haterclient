﻿using HaterClient.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using HaterClient.Models;
using HaterClient.Models.Enums;
using HaterClient.ViewModels;

namespace HaterClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationUserService _applicationUserServive;

        public HomeController()
        {
            _applicationUserServive = new ApplicationUserService();
        }
        public ActionResult Index()
        {
            GetToken();
            return View();
        }

        public string GetToken()
        {
            HttpCookie token = this.Request.Cookies["token"];
            if (token != null)
            {
                ViewBag.Token = token.Value;
                return token.Value;
            }
            return null;
        }

        public ActionResult Register()
        {
            var comps = from Gender d in Enum.GetValues(typeof(Gender))
                        select new { GenderString = d.ToString() };

            var vm = new RegisterVm()
            {
                PreferredGenderSelectList = new SelectList(comps, "GenderString", "GenderString"),
            };
            
            return View(vm);

        }

        [HttpPost]
        public async Task<ActionResult> Register(RegisterVm vm, string PreferredGenderSelectList)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser()
                {
                    Email = vm.Email,
                    Password = vm.Password,
                    ConfirmPassword = vm.ConfirmPassword,
                    Name = vm.Name,
                    PreferenceAgeStart = vm.AgaPreferenceStart,
                    PreferenceAgeStop = vm.AgePreferenceStop,
                    Birthday = vm.BirthDay,
                    Gender = vm.Gender,
                    CountryId = vm.Country,
                    PreferredGenders = new List<string>(){ PreferredGenderSelectList },
                };
                try
                {
                    await _applicationUserServive.AddNewApplicationUser(user, GetToken());
                    return RedirectToAction("Index");

                }
                catch (Exception e)
                {
                    vm.Error = "Laks katki";
                }
            }
            var comps = from Gender d in Enum.GetValues(typeof(Gender))
                        select new { GenderString = d.ToString() };

            vm.PreferredGenderSelectList = new SelectList(comps, "GenderString", "GenderString", PreferredGenderSelectList);
            return View(vm);
        }
    }
}