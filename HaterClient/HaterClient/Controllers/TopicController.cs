﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls.WebParts;
using HaterClient.Models;
using HaterClient.Services;
using HaterClient.ViewModels;

namespace HaterClient.Controllers
{
    public class TopicController : Controller
    {
        private readonly TopicService _topicService;
        private readonly VoteService _voteService;
        // GET: Topic
        public TopicController()
        {
            _topicService = new TopicService();
            _voteService = new VoteService();
        }

        public string GetToken()
        {
            HttpCookie token = this.Request.Cookies["token"];
            if (token != null)
            {
                ViewBag.Token = token.Value;
                return token.Value;
            }
            return null;
        }

        public async Task<ActionResult> Index()
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new AnswersTopicVm
            {
                Choices = new List<UserTopicChoice>()
            };

            try
            {
                vm.Choices = await _topicService.GetAll(ViewBag.Token);
            }
            catch (Exception e)
            {
                // ignored
            }
            //var topics = await _topicService.GetAllTopics(ViewBag.Token);
            return View(vm);
        }
       
        public ActionResult Create()
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new CreateTopicViewModel();
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CreateTopicViewModel vm)
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            if (!string.IsNullOrEmpty(vm.Topic))
            {
                var topic = new Topic()
                {
                    Description = vm.Topic,
                    CreatedAt = DateTime.Now,
                    Approved = true,
                };

                try
                {
                    await _topicService.Create(topic, GetToken());
                    vm = new CreateTopicViewModel()
                    {
                        Topic = "",
                        Message = "Your topic has been submitted!",
                        Success = true
                    };
                }
                catch (Exception e)
                {
                    vm.Message = "Topic has allready been submitted.";
                    vm.Success = false;
                }
            }
            else
            {
                vm.Message = "Please enter a topic";
                vm.Success = false;
            }

            return View(vm);
        }

        public async Task<ActionResult> Vote()
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new VoteViewModel();
            var topic = new VoteTopicGetViewModel();

            try
            {
                topic = await _voteService.Get(ViewBag.Token);
            }
            catch (Exception e)
            {
                // ignored
            }

            if (topic.TopicId > 0)
            {
                vm.TopicId = topic.TopicId;
                vm.Description = topic.Description;
            }
            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Vote(VoteViewModel vm)
        {
            GetToken();
            if (ViewBag.Token == null || String.IsNullOrEmpty(ViewBag.Token))
            {
                return RedirectToAction("Index", "Home");
            }

            if (vm.TopicId > 0)
            {
                var choice = new VoteTopicPostViewModel()
                {
                    TopicId = vm.TopicId,
                    Choice = vm.Choice
                };

                try
                {
                    await _voteService.Vote(choice, GetToken());
                }
                catch (Exception e)
                {
                    // ignored
                }
            }

            return RedirectToAction("Vote");
        }
    }
}