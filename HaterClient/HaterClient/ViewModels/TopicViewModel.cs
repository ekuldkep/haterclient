﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HaterClient.Models;
using HaterClient.Models.Enums;

namespace HaterClient.ViewModels
{
    public class CreateTopicViewModel
    {
        public string Topic { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }

    public class VoteViewModel
    {
        public int TopicId { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public Choice Choice { get; set; }
    }

    public class VoteTopicGetViewModel
    {
        public int TopicId { get; set; }
        public string Description { get; set; }
    }

    public class VoteTopicPostViewModel
    {
        public int TopicId { get; set; }
        public Choice Choice { get; set; }
    }


    public class AnswersTopicVm
    {
        public List<UserTopicChoice> Choices { get; set; }
        public UserTopicChoice Topic { get; set; }
        public Choice Choice { get; set; }
    }
}