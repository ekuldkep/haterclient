﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using HaterClient.Models.Enums;

namespace HaterClient.ViewModels
{
    [Bind(Exclude = "PreferredGenderSelectList")]
    public class RegisterVm
    {
        public string Error { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime BirthDay { get; set; }
        public Gender Gender { get; set; }
        public string Name { get; set; }
        public int Country{ get; set; }
        public int AgaPreferenceStart { get; set; }
        public int AgePreferenceStop { get; set; }
        public SelectList PreferredGenderSelectList{ get; set; }
    }
}