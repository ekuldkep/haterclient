﻿$(document).ready(function () {
    $('.choice-btn').click(function () {
        var btn = $(this);
        btn.parent().find('.choice-input').val(btn.attr('data-id'));
        btn.parents('.choice-form > form').submit();
    });
});