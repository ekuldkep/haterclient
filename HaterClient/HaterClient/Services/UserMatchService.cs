﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HaterClient.Models;

namespace HaterClient.Services
{
    public class UserMatchService:BaseService
    {
        public UserMatchService():base(ServiceConstants.MatchServiceUrl)
        {

        }

        public async Task<List<Models.UserMatch>> GetAllPersons()
        {
            return await base.GetData<List<UserMatch>>("", null);
        }

        public async Task<UserMatch> GetPersonById(int id)
        {
            return await base.GetData<UserMatch>(id.ToString(), null);

            //var responseMessage = await client.GetAsync("persons/"+id);
            //var responseContent = await responseMessage.Content.ReadAsStringAsync();
            //Person ret = await responseMessage.Content.ReadAsAsync<Person>();
            //return ret;
        }

        public async Task<UserMatch> AddNewPerson(UserMatch userMatch)
        {
            return await base.PostData<UserMatch>("", userMatch, null);
        }
    }
}