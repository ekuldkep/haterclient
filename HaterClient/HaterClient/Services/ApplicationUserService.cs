﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HaterClient.Models;

namespace HaterClient.Services
{
    public class ApplicationUserService:BaseService
    {
        public ApplicationUserService():base(ServiceConstants.AccountServiceUrl)
        {

        }

        public async Task<List<Models.ApplicationUser>> GetAllUsers(string token)
        {
            return await base.GetData<List<ApplicationUser>>("", token);
        }

        public async Task<ApplicationUser> GetUserById(int id, string token)
        {
            return await base.GetData<ApplicationUser>(id.ToString(), token);

            //var responseMessage = await client.GetAsync("persons/"+id);
            //var responseContent = await responseMessage.Content.ReadAsStringAsync();
            //Person ret = await responseMessage.Content.ReadAsAsync<Person>();
            //return ret;
        }

        public async Task<ApplicationUser> AddNewApplicationUser(ApplicationUser applicationUser, string token)
        {
            return await base.PostData<ApplicationUser>("Register", applicationUser, token);
        }
    }
}