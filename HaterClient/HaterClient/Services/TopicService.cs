﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HaterClient.Models;
using HaterClient.ViewModels;

namespace HaterClient.Services
{
    public class TopicService:BaseService
    {
        public TopicService():base(ServiceConstants.TopicsServiceUrl)
        {

        }
        
        public async Task<AnswersTopicVm> GetAll(string token)
        {
            return await base.GetData<AnswersTopicVm>("Answers/", token);
        }
        public async Task<Topic> GetById(int id, string token)
        {
            return await base.GetData<Topic>(id.ToString(), token);
        }

        public async Task<Topic> Create(Topic topic, string token)
        {
            return await base.PostData("", topic, token);
        }

        public async Task<VoteTopicPostViewModel> Vote(VoteTopicPostViewModel topic, string token)
        {
            return await base.PostData("", topic, token);
        }
    }
}