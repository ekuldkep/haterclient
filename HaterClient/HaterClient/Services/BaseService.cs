﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace HaterClient.Services
{
    public class BaseService
    {
        protected Uri _uri;

        public BaseService(string uri)
        {
            _uri = new Uri(uri);
        }

        public HttpClient clientFactory(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            if (!token.IsNullOrWhiteSpace())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            client.BaseAddress = _uri;
            return client;
        }

        protected async Task<T> GetData<T>(string urlPath, string token)
        {
            var client = clientFactory(token);

            HttpResponseMessage responseMessage = await client.GetAsync(urlPath);
            responseMessage.EnsureSuccessStatusCode();
            var responseJson = await responseMessage.Content.ReadAsStringAsync();

            T ret = await responseMessage.Content
                .ReadAsAsync<T>();

            return ret;
        }

        protected async Task<T> PostData<T>(string urlPath, T obj, string token)
        {
            var client = clientFactory(token);

            var response = await client.PostAsJsonAsync<T>(urlPath, obj);
            var responseJson = await response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<T>();
        }
    }
}