﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace HaterClient.Services
{
    public class ServiceConstants
    {
        private static string GetFullKey(string key)
        {
            return GetKey("ServiceUrl") + GetKey(key);
        }
        private static string GetKey(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
        
        public static string AccountServiceUrl => GetFullKey("Endpoint_Account");
        public static string MatchServiceUrl => GetFullKey("Endpoint_Match");
        public static string MessagesServiceUrl => GetFullKey("Endpoint_Messages");
        public static string TopicsServiceUrl => GetFullKey("Endpoint_Topics");
        public static string VoteServiceUrl => GetFullKey("Endpoint_Vote");
        public static string UserTopicChoicesServiceUrl => GetFullKey("Endpoint_UserTopicChoices");

    }
}