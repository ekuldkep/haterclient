﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using HaterClient.ViewModels;

namespace HaterClient.Services
{
    public class VoteService : BaseService
    {
        public VoteService() : base(ServiceConstants.VoteServiceUrl)
        {

        }

        public async Task<VoteTopicGetViewModel> Get(string token)
        {
            return await base.GetData<VoteTopicGetViewModel>("", token);
        }

        public async Task<VoteTopicPostViewModel> Vote(VoteTopicPostViewModel obj, string token)
        {
            return await base.PostData("", obj, token);
        }
    }
}