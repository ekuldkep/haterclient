﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using HaterClient.ViewModels;

namespace HaterClient.Services
{
    public class MatchService : BaseService
    {
        public MatchService() : base(ServiceConstants.MatchServiceUrl)
        {

        }

        public async Task<MatchUserViewModel> GetMatch(string token)
        {
            return await base.GetData<MatchUserViewModel>("", token);
        }

        public async Task<MatchUserViewModel> GetActive(string token)
        {
            return await base.GetData<MatchUserViewModel>("Active/", token);
        }

        public async Task<List<MatchUserViewModel>> GetPending(string token)
        {
            return await base.GetData<List<MatchUserViewModel>>("Pending/", token);
        }

        public async Task<int> Confirm(int id, string token)
        {
            return await base.PostData<int>(id.ToString(), id, token);
        }
    }
}